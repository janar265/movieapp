package com.example.jmanni.movieapp;

import com.example.jmanni.movieapp.exampledata.CategoryExampleData;
import com.example.jmanni.movieapp.moviedetail.MovieDataModel;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Objects;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;


/**
 * Created by Janar on 22-Apr-18.
 */

public class MovieDetailTest {


    @Mock private MovieDataModel testMovie;
    @Mock private CategoryExampleData categoryExampleData;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        testMovie = getTestMovie();
        categoryExampleData = new CategoryExampleData();
    }

    @Test
    public void checkAllFieldsTrue() {
        int movieId = 1;
        String movieTitle = "Test Movie";
        String movieDescription = "Test testMovie description";
        int movieYear = 2018;
        int movieRating = 5;
        int movieCategoryId = 2;
        String movieCategoryName = "Thriller";
        assertTrue(testMovie.getId() == movieId);
        assertTrue(Objects.equals(testMovie.getTitle(), movieTitle));
        assertTrue(testMovie.getDescription().equals(movieDescription));
        assertTrue(testMovie.getYear() == movieYear);
        assertTrue(testMovie.getRating() == movieRating);
        assertTrue(testMovie.getCategoryId() == movieCategoryId);
        assertTrue(categoryExampleData.getCategoryById(testMovie.getCategoryId()).equals(movieCategoryName));
    }

    @Test
    public void checkAllFieldsFalse() {
        int movieId = 2;
        String movieTitle = "Wrong Movie";
        String movieDescription = "Wrong testMovie description";
        int movieYear = 2012;
        int movieRating = 1;
        int movieCategoryId = 1;
        String movieCategoryName = "Drama";
        assertFalse(testMovie.getId() == movieId);
        assertFalse(Objects.equals(testMovie.getTitle(), movieTitle));
        assertFalse(testMovie.getDescription().equals(movieDescription));
        assertFalse(testMovie.getYear() == movieYear);
        assertFalse(testMovie.getRating() == movieRating);
        assertFalse(testMovie.getCategoryId() == movieCategoryId);
        assertFalse(categoryExampleData.getCategoryById(testMovie.getCategoryId()).equals(movieCategoryName));
    }

    private MovieDataModel getTestMovie() {
        return new MovieDataModel(1, "Test Movie", "Test testMovie description", 2018, 5, 2);
    }
}
