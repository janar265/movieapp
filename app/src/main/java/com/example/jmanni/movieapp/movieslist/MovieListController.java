package com.example.jmanni.movieapp.movieslist;

import android.app.Fragment;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.jmanni.movieapp.MainActivity;
import com.example.jmanni.movieapp.contentprovider.MovieService;
import com.example.jmanni.movieapp.moviedetail.MovieDetailController;

import java.util.ArrayList;

/**
 * Created by Janar on 17-Apr-18.
 *
 * Controller for movie list.
 */

public class MovieListController extends Fragment implements MovieListInteractor.MovieListInteractorListener {

    private MovieListView movieListView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        movieListView = new MovieListView(inflater, container, this);
        movieListView.setListener(this);
        return movieListView.getRootView();
    }

    /**
     *After the view is created this uses MovieService to load all the movies to the application.
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new MovieService() {
            @Override
            protected void onPostExecute(ArrayList arrayList) {
                movieListView.setUpMoviesRecycler(arrayList);
                Toast.makeText(getActivity().getBaseContext(),
                        "Movies loaded from local data list!", Toast.LENGTH_LONG).show();
            }
        }.execute();
    }

    /**
     * This will be called when clicked on movie item in movies list.
     */
    @Override
    public void onMovieItemClick(String movieID) {
        showMovie(movieID);
    }

    private void showMovie(String movieID) {
        Log.d("TAG", "CLICKED ON MOVIE: " + movieID);
        MovieDetailController movieDetail = MovieDetailController.newInstance(movieID);
        ((MainActivity) getActivity()).changeFragment(movieDetail);
    }
}
