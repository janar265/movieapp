package com.example.jmanni.movieapp.contentprovider;

import com.example.jmanni.movieapp.exampledata.MovieExampleData;
import com.example.jmanni.movieapp.moviedetail.MovieDataModel;

import java.util.ArrayList;

/**
 * Created by Janar on 20-Apr-18.
 *
 * This class has two methods that will return movie by id and list containign all movies.
 * Takes the data from example data. In real situation would take data from database.
 */

public class ContentProvider {

    private MovieExampleData movies = new MovieExampleData();

    public MovieDataModel findMovieById(String movieId) {
        return movies.getMoviesHash().get(movieId);
    }

    public ArrayList<MovieDataModel> getAllMovies() {
        return movies.getMoviesData();
    }
}
