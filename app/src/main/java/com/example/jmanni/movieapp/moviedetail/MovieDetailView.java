package com.example.jmanni.movieapp.moviedetail;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.jmanni.movieapp.R;
import com.example.jmanni.movieapp.exampledata.ImageSelector;

/**
 * Created by Janar on 18-Apr-18.
 */

public class MovieDetailView implements MovieDetailInteractor, MovieDetailInteractor.MovieDetailInteractorListener {

    private MovieDetailInteractorListener listener;
    private MovieDetailController controller;
    private ImageSelector imageSelector = new ImageSelector();
    private final View rootView;
    private ImageView moviePic;
    private TextView movieTitle;
    private TextView movieRating;
    private TextView movieDescription;
    private Button backButton;

    MovieDetailView(LayoutInflater inflater, ViewGroup container, final MovieDetailController controller) {
        this.controller = controller;
        rootView = inflater.inflate(R.layout.movie_detail, container, false);
        initialize();

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onBackButtonClick();
            }
        });
    }

    /**
     * Initialize all XML variables.
     */
    private void initialize() {
        moviePic = rootView.findViewById(R.id.movie_detail_pic);
        movieTitle = rootView.findViewById(R.id.movie_title);
        movieRating = rootView.findViewById(R.id.movie_rating);
        movieDescription = rootView.findViewById(R.id.movie_detail_description);
        backButton = rootView.findViewById(R.id.back_to_movies_btn);

    }

    /**
     * Bind movie details to the view.
     *
     * @param movie
     */
    @Override
    public void bindMovie(MovieDataModel movie) {
        Glide.with(controller.getActivity().getBaseContext())
                .load(imageSelector.getImageDrawable(movie.getTitle())).
                apply(RequestOptions.centerCropTransform())
                .into(moviePic);
        movieTitle.setText(movie.getTitle());
        movieRating.setText(String.valueOf(movie.getRating()));
        movieDescription.setText(movie.getDescription());
    }

    /**
     * On back button click action.
     */
    @Override
    public void onBackButtonClick() {

    }

    /**
     * Get the root view.
     * @return root Android View of this MVC View
     */
    @Override
    public View getRootView() {
        return rootView;
    }

    /**
     * @return Bundle containing the state of this MVC View, or null if the view has no state
     */
    @Override
    public Bundle getViewState() {
        return null;
    }

    /**
     * Set Listener for interactions.
     *
     * @param listener
     */
    @Override
    public void setListener(MovieDetailInteractorListener listener) {
        this.listener = listener;
    }
}
