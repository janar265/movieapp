package com.example.jmanni.movieapp.movieslist;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.jmanni.movieapp.R;
import com.example.jmanni.movieapp.exampledata.CategoryExampleData;
import com.example.jmanni.movieapp.exampledata.ImageSelector;
import com.example.jmanni.movieapp.moviedetail.MovieDataModel;

import java.util.ArrayList;

/**
 * Created by Janar on 17-Apr-18.
 *
 * Custom adapter for showing movies in recycler view.
 */

public class CustomMovieListAdapter extends RecyclerView.Adapter<CustomMovieListAdapter.ViewHolder> {

    private ArrayList<MovieDataModel> dataSet;
    private ItemClickListener clickListener;
    private CategoryExampleData categoryExample = new CategoryExampleData();
    private ImageSelector imageSelector = new ImageSelector();

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ImageView moviePic;
        TextView movieTitle;
        TextView movieReleaseYear;
        TextView movieCategory;
        TextView movieRating;

        ViewHolder(View itemView) {
            super(itemView);
            this.moviePic = itemView.findViewById(R.id.movie_card_pic);
            this.movieTitle = itemView.findViewById(R.id.movie_card_title);
            this.movieReleaseYear = itemView.findViewById(R.id.movie_card_year);
            this.movieCategory = itemView.findViewById(R.id.movie_card_category);
            this.movieRating = itemView.findViewById(R.id.movie_card_rating);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            clickListener.onItemClick(view);
        }
    }

    CustomMovieListAdapter(ArrayList<MovieDataModel> data) {
        this.dataSet = data;
    }

    /**
     * Empty card for inserting into the movie list.
     *
     * @param parent   The ViewGroup into which the new View will be added after it is bound to
     *                 an adapter position.
     * @param viewType The view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     * @see #getItemViewType(int)
     * @see #onBindViewHolder(ViewHolder, int)
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.movie_card, parent, false);
        return new ViewHolder(view);
    }

    /**
     * Set fields that will be shown in one single card of the movie list.
     *
     * @param holder   The ViewHolder which should be updated to represent the contents of the
     *                 item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ImageView moviePic = holder.moviePic;
        TextView movieTitle = holder.movieTitle;
        TextView movieReleaseYear = holder.movieReleaseYear;
        TextView movieCategory = holder.movieCategory;
        TextView movieRating = holder.movieRating;

        Glide.with(holder.itemView)
                .load(imageSelector.getImageDrawable(dataSet.get(position).getTitle()))
                .apply(RequestOptions.fitCenterTransform())
                .into(moviePic);

        movieTitle.setText(dataSet.get(position).getTitle());
        movieReleaseYear.setText(String.valueOf(dataSet.get(position).getYear()));
        movieCategory.setText(categoryExample.getCategoryById(dataSet.get(position).getCategoryId()));
        movieRating.setText(String.valueOf(dataSet.get(position).getRating()));
        holder.itemView.setTag(String.valueOf(dataSet.get(position).getId()));
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.clickListener = itemClickListener;
    }

    public interface ItemClickListener {
        void onItemClick(View view);
    }

}
