package com.example.jmanni.movieapp.moviedetail;

import android.os.Bundle;
import android.view.View;

/**
 * Created by Janar on 18-Apr-18.
 */

public interface MovieDetailInteractor {

    interface MovieDetailInteractorListener {
        /**
         * On backbutton click action.
         */
        void onBackButtonClick();

        /**
         *Get the root view.
         * @return root Android View of this MVC View
         */
        View getRootView();

        /**
         * @return Bundle containing the state of this MVC View, or null if the view has no state
         */
        Bundle getViewState();
    }
    /**
     * Bind movie details to the view.
     */
    void bindMovie(MovieDataModel movie);

    /**
     * Set Listener for interactions.
     */
    void setListener(MovieDetailInteractorListener listener);
}
