package com.example.jmanni.movieapp.movieslist;

import android.os.Bundle;
import android.view.View;

/**
 * Created by Janar on 17-Apr-18.
 *
 * Interface for Movie List
 */

public interface MovieListInteractor {

    interface MovieListInteractorListener {
        /**
         * This will be called when clicked on movie item in movies list.
         */
        void onMovieItemClick(String movieID);
    }

    /**
     * Listener for movie list.
     */
    void setListener(MovieListInteractorListener listener);

    /**
     * Get the root view.
     * @return root Android View of this MVC View
     */
    View getRootView();

    /**
     * @return Bundle containing the state of this MVC View, or null if the view has no state
     */
    Bundle getViewState();
}
