package com.example.jmanni.movieapp.contentprovider;

import android.os.AsyncTask;

import com.example.jmanni.movieapp.moviedetail.MovieDataModel;

/**
 * Created by Janar on 22-Apr-18.
 *
 *
 */

public abstract class MovieServiceById extends AsyncTask<String, Void, MovieDataModel> {

    ContentProvider contentProvider = new ContentProvider();

    @Override
    protected MovieDataModel doInBackground(String... strings) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return contentProvider.findMovieById(strings[0]);
    }

    @Override
    protected void onPostExecute(MovieDataModel movie) {
        super.onPostExecute(movie);
    }
}
