package com.example.jmanni.movieapp.exampledata;

import com.example.jmanni.movieapp.R;

/**
 * Created by Janar on 20-Apr-18.
 *
 * Image selector to make this application visually nicer. Later I am using Glide library to load
 * the pictures. It makes it faster and Glide also makes caching picture data easy. Thanks to Glide
 * the pictures in Movie List recycler view should not lag, the scrolling motion should be smooth.
 */

public class ImageSelector {

    public int getImageDrawable(String movieTitle) {
        switch (movieTitle) {
            case "Pulp Fiction":
                return R.drawable.pulp_fiction;
            case "Titanic":
                return R.drawable.titanic;
            case "The Dark Knight":
                return R.drawable.the_dark_knight;
            case "Forrest Gump":
                return R.drawable.forrest_gump;
            case "The Godfather":
                return R.drawable.the_godfather;
            case "The Godfather II":
                return R.drawable.the_godfather2;
            default:
                return R.drawable.forrest_gump;
        }
    }
}
