package com.example.jmanni.movieapp.movieslist;

/**
 * Created by Janar on 17-Apr-18.
 *
 * Started 12 : 07
 *
 * Movie list data model. Everything movies list shows.
 */

public class MovieListDataModel {

    private String title;
    private int rating;
    private String category;
    private int year;

    public MovieListDataModel(String title, int rating, String category, int year) {
        this.title = title;
        this.rating = rating;
        this.category = category;
        this.year = year;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
