package com.example.jmanni.movieapp.exampledata;

/**
 * Created by Janar on 17-Apr-18.
 *
 * Started 12 : 09
 *
 * Data model for movie categories.
 */

public class CategoryData {

    private int id;
    private String category;

    public CategoryData(int id, String category) {
        this.id = id;
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
