package com.example.jmanni.movieapp.movieslist;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jmanni.movieapp.contentprovider.ContentProvider;
import com.example.jmanni.movieapp.moviedetail.MovieDataModel;
import com.example.jmanni.movieapp.R;

import java.util.ArrayList;

/**
 * Created by Janar on 17-Apr-18.
 *
 * Movie list view.
 */

public class MovieListView implements MovieListInteractor, CustomMovieListAdapter.ItemClickListener {

    private final View rootView;
    private MovieListController controller;
    private ContentProvider contentProvider = new ContentProvider();
    private ArrayList<MovieDataModel> movies;
    private CustomMovieListAdapter moviesAdapter;
    private MovieListInteractorListener listener;

    MovieListView(LayoutInflater inflater, ViewGroup container, MovieListController controller) {
        rootView = inflater.inflate(R.layout.movie_list_view, container, false);
        this.controller = controller;
    }

    /**
     * Listener for movie list.
     *
     * @param listener
     */
    @Override
    public void setListener(MovieListInteractorListener listener) {
        this.listener = listener;
    }

    /**
     * Set up recycler view for displaying movies.
     *
     * @param movies list of movies to show.
     */
    public void setUpMoviesRecycler(ArrayList<MovieDataModel> movies) {
        RecyclerView moviesRecycler = rootView.findViewById(R.id.movies_list);
        moviesRecycler.setHasFixedSize(false);

        RecyclerView.LayoutManager moviesManager = new LinearLayoutManager(controller.getActivity());
        moviesRecycler.setLayoutManager(moviesManager);

        moviesAdapter = new CustomMovieListAdapter(movies);
        moviesAdapter.setClickListener(this);

        moviesRecycler.setAdapter(moviesAdapter);

    }
    /**
     *Get the root view.
     * @return root Android View of this MVC View
     */
    @Override
    public View getRootView() {
        return rootView;
    }

    /**
     * @return Bundle containing the state of this MVC View, or null if the view has no state
     */
    @Override
    public Bundle getViewState() {
        return null;
    }

    @Override
    public void onItemClick(View view) {
        if (listener != null) {
            listener.onMovieItemClick((String) view.getTag());
        }
    }
}
