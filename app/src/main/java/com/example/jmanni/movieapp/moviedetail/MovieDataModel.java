package com.example.jmanni.movieapp.moviedetail;

/**
 * Created by Janar on 17-Apr-18.
 *
 * Started 12 : 04
 *
 * Movie data model class. Everything that movies has.
 */

public class MovieDataModel {

    private int id;
    private String title;
    private String description;
    private int year;
    private int rating;
    private int categoryId;

    public MovieDataModel(int id, String title, String description, int year, int rating, int categoryId) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.year = year;
        this.rating = rating;
        this.categoryId = categoryId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
}
