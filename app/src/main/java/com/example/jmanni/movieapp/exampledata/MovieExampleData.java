package com.example.jmanni.movieapp.exampledata;

import com.example.jmanni.movieapp.moviedetail.MovieDataModel;

import java.util.ArrayList;
import java.util.HashMap;


/**
 * Created by Janar on 17-Apr-18.
 *
 * Example data for this app. It doesn't contain the picture reference or information. The picture
 * for a movie comes from ImageSelector class. In real application the database should contain the
 * reference to the picture also.
 */

public class MovieExampleData {

    private HashMap<String, MovieDataModel> moviesHash = new HashMap<>();
    private ArrayList<MovieDataModel> movies;
    private final String description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam luctus sem nec lectus pulvinar vestibulum." +
            " Donec nec venenatis ligula. Nunc a lorem vel erat facilisis mattis quis ac risus. Vivamus magna augue, consequat nec arcu eu, condimentum luctus sapien." +
            " Maecenas vel sodales risus. Phasellus ullamcorper, massa vulputate bibendum consequat, lectus nibh aliquam massa, in volutpat felis dolor sed enim." +
            " Suspendisse elementum varius leo, eu lobortis sem ullamcorper eu. Maecenas porta iaculis quam quis venenatis. Nulla id arcu ac elit tempor iaculis.\n" +
            "Nullam nec massa quis ligula laoreet ornare a ut nisl. Quisque at urna cursus, aliquet risus in, auctor metus. Phasellus pulvinar luctus purus, et tempor libero blandit nec." +
            " Quisque feugiat, leo eu commodo fringilla, leo quam varius turpis, sed pellentesque mi neque sed nulla. Quisque in euismod nunc. Nulla aliquam posuere urna," +
            " at vulputate neque tincidunt nec. Nunc tincidunt metus sodales nisi laoreet, non euismod tellus ullamcorper. Cras eu leo quis diam cursus condimentum." +
            " Quisque tincidunt arcu a enim condimentum, et consectetur risus semper. Fusce pharetra vestibulum risus. Quisque ornare augue non felis gravida fringilla" +
            " sit amet in mi. Sed ut dui aliquam, efficitur ante quis, cursus quam.";

    public MovieExampleData() {
        moviesHash.put("1", new MovieDataModel(1, "The Godfather", description, 1972, 5, 1));
        moviesHash.put("2", new MovieDataModel(2, "The Dark Knight", description, 2008, 5,2));
        moviesHash.put("3", new MovieDataModel(3, "The Godfather II", description, 1974, 2, 4));
        moviesHash.put("4", new MovieDataModel(4, "Pulp Fiction", description, 1994, 3, 2));
        moviesHash.put("5", new MovieDataModel(5, "Forrest Gump", description, 1994, 3, 1));
        moviesHash.put("6", new MovieDataModel(6, "Titanic", description, 1997, 3, 1));
        moviesHash.put("7", new MovieDataModel(7, "The Godfather", description, 1972, 5, 1));
        moviesHash.put("8", new MovieDataModel(8, "The Dark Knight", description, 2008, 5,2));
        moviesHash.put("9", new MovieDataModel(9, "The Godfather II", description, 1974, 2, 3));
        moviesHash.put("100", new MovieDataModel(100, "Pulp Fiction", description, 1994, 3, 2));
        moviesHash.put("11", new MovieDataModel(11, "Forrest Gump", description, 1994, 3, 1));
        moviesHash.put("12", new MovieDataModel(12, "Titanic", description, 1997, 3, 1));
        movies = new ArrayList<>(moviesHash.values());
    }

    public HashMap<String, MovieDataModel> getMoviesHash() {
        return moviesHash;
    }

    public ArrayList<MovieDataModel> getMoviesData() {
        return movies;
    }

}
