package com.example.jmanni.movieapp.exampledata;

import java.util.ArrayList;

/**
 * Created by Janar on 20-Apr-18.
 *
 * Example category information. And logic to get the readable category name in application.
 */

public class CategoryExampleData {

    private ArrayList<CategoryData> categories = new ArrayList<>();

    public CategoryExampleData() {
        categories.add(new CategoryData(1, "Drama"));
        categories.add(new CategoryData(2, "Thriller"));
        categories.add(new CategoryData(3, "Comedy"));
    }

    public String getCategoryById(int categoryId) {
        String returnedCategory = "";
        for (CategoryData category : categories) {
            if (category.getId() == categoryId) {
                return category.getCategory();
            } else {
                returnedCategory = "No Such Category!";
            }
        }
        return returnedCategory;
    }
}
