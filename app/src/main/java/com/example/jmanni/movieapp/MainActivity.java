package com.example.jmanni.movieapp;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.example.jmanni.movieapp.movieslist.MovieListController;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toast.makeText(this, "Loading data from local data list. It takes 1 second."
                , Toast.LENGTH_SHORT).show();
        setContentView(R.layout.activity_main);
        MovieListController moviesListController = new MovieListController();
        changeFragment(moviesListController);
    }

    public void changeFragment(Fragment fragment) {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.MainFrame, fragment).addToBackStack("s").commit();
    }
}
