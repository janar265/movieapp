package com.example.jmanni.movieapp.contentprovider;

import android.os.AsyncTask;

import java.util.ArrayList;

/**
 * Created by Janar on 20-Apr-18.
 */

public abstract class MovieService extends AsyncTask<Void, Void, ArrayList> {

    private ContentProvider contentProvider = new ContentProvider();

    @Override
    protected ArrayList doInBackground(Void... voids) {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return contentProvider.getAllMovies();
    }

    @Override
    protected void onPostExecute(ArrayList arrayList) {
        super.onPostExecute(arrayList);
    }

}
