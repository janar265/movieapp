package com.example.jmanni.movieapp.moviedetail;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.jmanni.movieapp.contentprovider.MovieServiceById;


/**
 * Created by Janar on 18-Apr-18.
 */

public class MovieDetailController extends Fragment implements MovieDetailInteractor.MovieDetailInteractorListener {

    private static final String ARG_MOVIE_ID = "movie_id";
    private MovieDetailView movieView;
    private String movieId;


    public static MovieDetailController newInstance(String movieId) {
        MovieDetailController movieDetail = new MovieDetailController();
        Bundle args = new Bundle();
        args.putString(ARG_MOVIE_ID, movieId);
        movieDetail.setArguments(args);
        return movieDetail;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        Bundle args = getArguments();
        movieId = args.getString(ARG_MOVIE_ID);
        movieView = new MovieDetailView(inflater, container, this);
        movieView.setListener(this);
        return movieView.getRootView();
    }

    /**
     *After the view is created load movie into the view.
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new MovieServiceById() {
            @Override
            protected void onPostExecute(MovieDataModel movie) {
                movieView.bindMovie(movie);
                Toast.makeText(getActivity().getBaseContext(),
                        "Loaded movie data from hardcoded list!", Toast.LENGTH_SHORT).show();
            }
        }.execute(movieId);
    }

    /**
     * On backbutton click action.
     */
    @Override
    public void onBackButtonClick() {
        Log.d("BAKCBUTTON", "Back button clicked.");
        getActivity().getFragmentManager().popBackStack();
    }

    /**
     * Get the root Android View which is used internally by this MVC View for presenting data
     * to the user.<br>
     * The returned Android View might be used by an MVC Controller in order to query or alter the
     * properties of either the root Android View itself, or any of its child Android View's.
     *
     * @return root Android View of this MVC View
     */
    @Override
    public View getRootView() {
        return null;
    }

    /**
     * This method aggregates all the information about the state of this MVC View into Bundle
     * object. The keys in the returned Bundle must be provided as public constants inside the
     * interfaces (or implementations if no interface defined) of concrete MVC views.<br>
     * The main use case for this method is exporting the state of editable Android Views underlying
     * the MVC view. This information can be used by MVC controller for e.g. processing user's
     * input or saving view's state during lifecycle events.
     *
     * @return Bundle containing the state of this MVC View, or null if the view has no state
     */
    @Override
    public Bundle getViewState() {
        return null;
    }
}
