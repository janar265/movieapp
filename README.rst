Movieapp
------------

This is the application for internship position in CGI Eesti. In the application you can scroll
through movies and select preferred movie to read more about it.
The project took about 15-20 hours of work. Started in the morning of 17th April and worked almost
the whole day. 18th April worked in the evening, 20th April worked half a day. From morning until 17.00
And 22th April worked half a day. Those are rough measurements, should make up to 15-20 hours of work.

Tested on Samsung s6 and virtual machine Nexus 5X both devices operated normally in both portrait and
landscape modes.

In this project im also using Glide library for showing and loading images into imageviews. This helps
the recycler view to scroll smooth. And caching options.


Started 17.04.18 11.40.

Movie List recycle view.

18.04.18 21.30.
Added movie detail view.

20.04.18
Design changes. Started on content provider.

22.04.18
Added AsyncTasks.

Ended 22.04.18 23.05